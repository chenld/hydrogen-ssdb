package com.hyd.ssdb;

/**
 * (description)
 * created at 15-12-3
 *
 * @author Yiding
 */
public class Restrictions {

    /**
     * 位操作的最大位置
     */
    public static final long MAX_BIT_OFFSET = 1073741824;
}
